%{
	#include "tabsim.c"
    #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>
    void yyerror(char *);
    int yylex(void);
%}


%union{
	char *string;
	int entero;
	struct _info *i;
}

%token <string>IDENTIFIER
%token	<string>I_CONSTANT F_CONSTANT STRING_LITERAL FUNC_NAME SIZEOF
%token	PTR_OP INC_OP DEC_OP LEFT_OP RIGHT_OP LE_OP GE_OP EQ_OP NE_OP
%token	AND_OP OR_OP MUL_ASSIGN DIV_ASSIGN MOD_ASSIGN ADD_ASSIGN
%token	SUB_ASSIGN LEFT_ASSIGN RIGHT_ASSIGN AND_ASSIGN
%token	XOR_ASSIGN OR_ASSIGN
%token	TYPEDEF_NAME ENUMERATION_CONSTANT

%token	TYPEDEF EXTERN STATIC AUTO REGISTER INLINE
%token	CONST RESTRICT VOLATILE
%token	<string> BOOL CHAR SHORT INT LONG SIGNED UNSIGNED FLOAT DOUBLE VOID
%token	COMPLEX IMAGINARY 
%token	STRUCT UNION ENUM ELLIPSIS

%token	<string> CASE DEFAULT IF ELSE SWITCH WHILE DO FOR GOTO CONTINUE BREAK RETURN

%token	ALIGNAS ALIGNOF ATOMIC GENERIC NORETURN STATIC_ASSERT THREAD_LOCAL
%type <string>    declaration_specifiers
%type <string> type_specifier program translation_unit external_declaration
%type <string> declaration function_definition  declare_  compound_statement
%type <string> start_scope end_scope block_item_list block_item statement selection_statement
%type <string> expression asignacion iteration_statement expression_statement call_function double_int
%type <i> operacion
%start  program
%%

program
	:  translation_unit {printf(" public class Main { %s }\n",$1);}
	;

translation_unit
	: external_declaration {$$ = $1;}
	| translation_unit external_declaration { $$ = strcat($1,$2); }
	;

external_declaration
	: function_definition {$$ = $1;}
	| declaration {$$ = $1;}
	;

declaration
	: declaration_specifiers IDENTIFIER ';' 
		{
		char *cad = (char *)malloc(200); strcat(cad,"el identificador: \"");strcat(cad,$2);strcat(cad, "\" ya existe");
		if(existe($2,1)){yyerror(cad);}else{addSymbol($2);setType($1);} 
		//generacion de codigo
		$$ = strcat(strcat(strcat($1," "),$2),";");
		}
	| declaration_specifiers IDENTIFIER '=' IDENTIFIER ';'
		{
		if(existe($2,1)){
		char *cad = (char *)malloc(200); strcat(cad,"el identificador: \"");strcat(cad,$2);strcat(cad, "\" ya existe");
		yyerror(cad);
		}else{addSymbol($2);setType($1);} 
		if(!existe($4,0)){
		char *cad = (char *)malloc(200); strcat(cad,"el identificador: \"");strcat(cad,$4);strcat(cad, "\" no existe");
		yyerror(cad);
		}
		
		if(!strcmp(getType($4),$1))
		{
		}else
			if(!strcmp($1,"char") && (!strcmp(getType($4),"int") || !strcmp(getType($4),"double") || !strcmp(getType($4),"short")))
			{
			   char *cad = (char *)malloc(1000);
			   strcat(cad,"char ");
			   strcat(cad,$2);
			   strcat(cad,"=");
			   strcat(cad,"(char)");
			   strcat(cad,$4);
			   strcat(cad,";");
				$$ = cad;
			}else
			if(!strcmp($1,"short") && (!strcmp(getType($4),"int") || !strcmp(getType($4),"double")))
			{
			   char *cad = (char *)malloc(1000);
			   strcat(cad,"short ");
			   strcat(cad,$2);
			   strcat(cad,"=");
			   strcat(cad,"(short)");
			   strcat(cad,$4);
			   strcat(cad,";");
				$$ = cad;
			}else
			if(!strcmp($1,"int") && (!strcmp(getType($4),"double")))
			{
				 char *cad = (char *)malloc(1000);
			   strcat(cad,"int ");
			   strcat(cad,$2);
			   strcat(cad,"=");
			   strcat(cad,"(int)");
			   strcat(cad,$4);
			   strcat(cad,";");
			   
				$$ = cad;
				
			}else
			if(!strcmp($1,"double") && (!strcmp(getType($4),"int") || !strcmp(getType($4),"char") ||!strcmp(getType($4),"short")))
			{
				$$ = strcat(strcat(strcat(strcat(strcat($1," "),$2),"="),$4),";");
			} else
			if(!strcmp($1,"int") && (!strcmp(getType($4),"short") || !strcmp(getType($4),"char")))
			{
				$$ = strcat(strcat(strcat(strcat(strcat($1," "),$2),"="),$4),";");
			}else
			{
				yyerror("incompatibilidad de tipos");
			}
		//generacion de codigo
		
		} 
	| declaration_specifiers IDENTIFIER '=' I_CONSTANT ';'
		{
		if(existe($2,1)){
		char *cad = (char *)malloc(200); strcat(cad,"el identificador: \"");strcat(cad,$2);strcat(cad, "\" ya existe");
		yyerror(cad);
		}else{addSymbol($2);setType($1);}
		if(!strcmp($1,"int") || !strcmp($1,"double") || !strcmp($1,"char") || !strcmp($1,"short")){
			
		}else{
				yyerror("incompatibilidad de tipos");
			}
		//generacion de codigo
		$$ = strcat(strcat(strcat(strcat(strcat($1," "),$2),"="),$4),";");
		}
		
	| declaration_specifiers IDENTIFIER '=' F_CONSTANT ';'
		{
		if(existe($2,1)){
		char *cad = (char *)malloc(200); strcat(cad,"el identificador: \"");strcat(cad,$2);strcat(cad, "\" ya existe");
		yyerror(cad);
		}else{addSymbol($2);setType($1);}
		if(!strcmp($1,"int") || !strcmp($1,"double") || !strcmp($1,"char") || !strcmp($1,"short")){
		if(!strcmp($1,"int") || !strcmp($1,"char") || !strcmp($1,"short")){
		char *dec = (char *)malloc(strlen($1)+1);
		strcpy(dec,$1);
		strcat($1," ");
		strcat($1,$2);
		strcat($1,"=");
		strcat($1,"(");
		strcat($1,dec);
		strcat($1,")");
		strcat($1,$4);
		strcat($1,";");
		$$ = $1;
		}else{$$ = strcat(strcat(strcat(strcat(strcat($1," "),$2),"="),$4),";");}
		}else{
			yyerror("tipos incorrectos");
			}
		
		
		
		}
		
	| declaration_specifiers IDENTIFIER '=' STRING_LITERAL ';'
		{
		if(existe($2,1)){
		char *cad = (char *)malloc(200); strcat(cad,"el identificador: \"");strcat(cad,$2);strcat(cad, "\" ya existe");
		yyerror(cad);
		}else{addSymbol($2);setType($1);}
		if(!(!strcmp($1,"int") || !strcmp($1,"double") || !strcmp($1,"char") || !strcmp($1,"short"))){
			
		}else{
			yyerror("tipos incorrectos");
			}
		
		}
	| declaration_specifiers '*' IDENTIFIER '=' STRING_LITERAL ';' {
		if(existe($3,1)){
		char *cad = (char *)malloc(200); strcat(cad,"el identificador: \"");strcat(cad,$3);strcat(cad, "\" ya existe");
		yyerror(cad);
		}else{addSymbol($3);setType(strcat($1,"*"));} 
		
			if(strcmp(getType($3),"char*"))
			{
			yyerror("tipos incorrectos");
			}
			//generacion de codigo
			char *cad = (char *) malloc(1000);
			strcat(cad,"String ");
			strcat(cad,$3);strcat(cad," = ");strcat(cad,$5);strcat(cad,";");
			$$ = cad;
		}
	| declaration_specifiers '*' IDENTIFIER '=' I_CONSTANT ';' {
		if(existe($3,1)){
		char *cad = (char *)malloc(200); strcat(cad,"el identificador: \"");strcat(cad,$3);strcat(cad, "\" ya existe");
		yyerror(cad);
		}else{addSymbol($3);setType(strcat($1,"*"));}
		//TODO
		if(atoi($5) == 0){
		char *cad = (char *)malloc(1000);
		if(!strcmp($1,"int*"))strcat(cad,"int "); 
		if(!strcmp($1,"double*"))strcat(cad,"double "); 
		if(!strcmp($1,"short*"))strcat(cad,"short "); 
		if(!strcmp($1,"char*"))strcat(cad,"char "); 
		
			strcat(cad, $3);
			strcat(cad, "=");
			strcat(cad, $5);
			strcat(cad, ";");
			$$ = cad;
		}else
				yyerror("tipos incorrectos");
		}
	| declaration_specifiers '*' IDENTIFIER '=' IDENTIFIER ';' {
		if(existe($3,1)){
		char *cad = (char *)malloc(200); strcat(cad,"el identificador: \"");strcat(cad,$3);strcat(cad, "\" ya existe");
		yyerror(cad);
		}else{addSymbol($3);setType(strcat($1,"*"));}
		
		//TODO
		if(strcmp($1,getType($5))){
			yyerror("tipos invalidos");
		}else{
			char *cad = (char *)malloc(1000);
			if(!strcmp($1,"int*"))strcat(cad,"int "); 
			if(!strcmp($1,"double*"))strcat(cad,"double "); 
			if(!strcmp($1,"short*"))strcat(cad,"short "); 
			if(!strcmp($1,"char*"))strcat(cad,"String "); 
		
			strcat(cad, $3);
			strcat(cad, "=");
			strcat(cad, $5);
			strcat(cad, ";");
			$$ = cad;
		}
			
		}
	| declaration_specifiers '*' IDENTIFIER';' {
		if(existe($3,1)){
		char *cad = (char *)malloc(200); strcat(cad,"el identificador: \"");strcat(cad,$3);strcat(cad, "\" ya existe");
		yyerror(cad);
		}else{addSymbol($3);setType(strcat($1,"*"));} $$ = $1;
		
			char *cad = (char *) malloc(1000);
			//generacion de codigo
			if(!strcmp($1,"int*")){
				strcat(cad,"int ");
				$$ = strcat(strcat(cad,$3),";");
			}else if(!strcmp($1,"double*")){
				strcat(cad,"double ");
				$$ = strcat(strcat(cad,$3),";");
			}else if(!strcmp($1,"short*")){
				strcat(cad,"short ");
				$$ = strcat(strcat(cad,$3),";");
			}else if(!strcmp($1,"char*")){
				strcat(cad,"String ");
				$$ = strcat(strcat(cad,$3),";");
			}
		}
	;
	
 declaration_specifiers
 	:type_specifier { $$ = $1; }
 	;
type_specifier
	: VOID { $$ = $1;}
	| CHAR { $$ = $1;}
	| SHORT { $$ = $1;}
	| INT { $$ = $1;}
	| LONG { $$ = $1;}
	| FLOAT { $$ = $1;}
	| DOUBLE { $$ = $1;}
	;
function_definition
	: declare_ compound_statement	{$$ = strcat($1,$2);}
	;
	
	/**ajuste creado para la deficion de metodos**/
declare_
	:declaration_specifiers IDENTIFIER '('')'{$$ = strcat(strcat(strcat($1," "),$2),"()");}
	;

compound_statement
	: start_scope end_scope { $$ = strcat($1,$2);}
	| start_scope  block_item_list end_scope {
	char * cad = (char *)malloc(1000); strcat(cad,"{");  $$ = strcat(strcat(cad,$2),"}");}
	;
	
/** ajuste para el control de ambitos **/
start_scope
	: '{' {push(); char * cad = (char *)malloc(10000); $$ = strcat(cad,"{");} 
	;
end_scope
	: '}' {pop();  char * cad = (char *)malloc(1000); $$ = strcat(cad,"}");}
	;

block_item_list
	: block_item {$$ = $1;}
	| block_item_list block_item { $$= strcat($1,$2);}
	;

block_item
	: declaration {$$ = $1;}
	| statement {$$ = $1;}
	;

statement
	: compound_statement { $$ = $1 ;}
	|selection_statement {$$ = $1;}
	|iteration_statement
	|expression {$$ = $1;}
	|call_function ';'{$$ = strcat($1,";"); }
	;
call_function
:	IDENTIFIER '(' IDENTIFIER ')'
{
	if(!strcmp($1,"strlen")){
		if(!strcmp(getType($3),"char*")){
			char * cad = (char *) malloc(100);
			strcat(cad,$3);strcat(cad,".length()");
			$$ = cad;
		}else
			yyerror("argumento incorrecto");
	}if(!strcmp($1,"sqrt")){
		if(existe($3,0)){
			if(!(!strcmp(getType($3),"int") || !strcmp(getType($3),"double")))
				yyerror("argumento incompatible");
		}
		char *cad = (char * )malloc(1000);
		strcat(cad,"Math.sqrt(");strcat(cad,$3);strcat(cad,")");
		$$ = cad;
	}if(!strcmp($1,"strcmp")){
		yyerror("faltan argumentos");
	}
}
| IDENTIFIER '(' IDENTIFIER ',' IDENTIFIER ')'
{
	if(!strcmp($1,"strcmp")){
		if(strcmp(getType($3),"char*")){
		yyerror("argumento incorrecto");
		}
		if(strcmp(getType($5),"char*")){
			yyerror("argumento incorrecto");
		}
			char * cad = (char *) malloc(100);
			strcat(cad,$3);strcat(cad,".equals(");strcat(cad,$5);strcat(cad,")");
			$$ = cad;
		
			
	}
}
|IDENTIFIER '(' STRING_LITERAL ',' IDENTIFIER ')'	
{
	if(!strcmp($1,"printf")){
		char * cad = (char *) malloc(100);
		strcat(cad,"System.out.printf(");strcat(cad,$3);strcat(cad,",");strcat(cad,$5);strcat(cad,")");
		$$ = cad;
	}
}
|IDENTIFIER '(' double_int ')'
{
	
		char *cad = (char * )malloc(1000);
		strcat(cad,"Math.sqrt(");strcat(cad,$3);strcat(cad,")");
		$$ = cad;
	
}
;
double_int
: F_CONSTANT { $$ = $1; }
| I_CONSTANT { $$ =  $1; }
;
selection_statement
	: IF '(' expression ')' statement ELSE statement {$$ = strcat(strcat(strcat(strcat(strcat(strcat($1,"("),$3),")"),$5),strcat($6," ")),$7);}
	| IF '(' expression ')' statement {$$ = strcat(strcat(strcat(strcat($1,"("),$3),")"),$5);}
	;
expression
	: IDENTIFIER 
	{
	if(!existe($1,0)){
		char *cad = (char *)malloc(200); strcat(cad,"el identificador: \"");strcat(cad,$1);strcat(cad, "\" no existe");
		yyerror(cad);
		}
	$$ = $1;
	}
	|asignacion  {$$ = $1;}
	| I_CONSTANT 
	{ 
		if(atoi($1) == 0)
		{
			$$ = "false";
		}else $$ = "true";
	}
	| IDENTIFIER '(' IDENTIFIER ')'
	{
		if(!strcmp($1,"strlen")){
			if(strcmp(getType($3),"char*")){
			yyerror("argumento incorrecto");
			}
				char * cad = (char *) malloc(100);
				strcat(cad,$3);strcat(cad,".isEmpty()");
				$$ = cad;
		}else
			yyerror("metodo no existe");
	}

	;
asignacion
	: IDENTIFIER '=' IDENTIFIER ';' 
	{
		if(!existe($1,0)){
		char *cad = (char *)malloc(200);strcat(cad,""); strcat(cad,"el identificador: \"");strcat(cad,$1);strcat(cad, "\" no existe");
		yyerror(cad); }
		if(!existe($3,0)){
		char *cad = (char *)malloc(200); strcat(cad,"el identificador: \"");strcat(cad,$3);strcat(cad, "\" no existe");
		yyerror(cad);
		} 
		
		if(!strcmp(getType($1),getType($3))){
			$$ = strcat(strcat(strcat($1,"="),$3),";");
		}else if(!strcmp(getType($1),"char") && (!strcmp(getType($3),"int") || !strcmp(getType($3),"double") || !strcmp(getType($3),"short")))
			{
			   char *cad = (char *)malloc(1000);
			   strcat(cad,$1);
			   strcat(cad,"=");
			   strcat(cad,"(char)");
			   strcat(cad,$3);
			   strcat(cad,";");
				$$ = cad;
			}else
			if(!strcmp(getType($1),"short") && (!strcmp(getType($3),"int") || !strcmp(getType($3),"double")))
			{
				char *cad = (char *)malloc(1000);
			   strcat(cad,$1);
			   strcat(cad,"=");
			   strcat(cad,"(short)");
			   strcat(cad,$3);
			   strcat(cad,";");
				$$ = cad;
			}else
			if(!strcmp(getType($1),"int") && (!strcmp(getType($3),"double")))
			{
				char *cad = (char *)malloc(1000);
			   strcat(cad,$1);
			   strcat(cad,"=");
			   strcat(cad,"(int)");
			   strcat(cad,$3);
			   strcat(cad,";");
				$$ = cad;
			}else
			if(!strcmp(getType($1),"double") && (!strcmp(getType($3),"int") || !strcmp(getType($3),"char") ||!strcmp(getType($3),"short")))
			{
				$$ = strcat(strcat(strcat($1,"="),$3),";");
			} else
			if(!strcmp(getType($1),"int") && (!strcmp(getType($3),"short") || !strcmp(getType($3),"char")))
			{
				$$ = strcat(strcat(strcat($1,"="),$3),";");
			}else{
				yyerror("tipos incorrectos");
			}
		
		
		
		
	}
	|IDENTIFIER '=' STRING_LITERAL ';'{
		if(!existe($1,0)) {
		char *cad = (char *)malloc(200); strcat(cad,"el identificador: \"");strcat(cad,$1);strcat(cad, "\" no existe");
		yyerror(cad);
		}
		if(!strcmp(getType($1),"char*"))
		{
			$$ = strcat(strcat(strcat($1,"="),$3),";");
		}else{
		yyerror("tipos incorrectos");
		}
	}
	|IDENTIFIER '=' I_CONSTANT ';'{
		if(!existe($1,0)) {
		char *cad = (char *)malloc(200); strcat(cad,"el identificador: \"");strcat(cad,$1);strcat(cad, "\" no existe");
		yyerror(cad);
		}
		if(!strcmp(getType($1),"int") || !strcmp(getType($1),"double") || !strcmp(getType($1),"char"))
		{
			$$ = strcat(strcat(strcat($1,"="),$3),";");
		}else{
		yyerror("tipos incorrectos");
		}
	}
	|IDENTIFIER '=' operacion ';'{ 
		if(!existe($1,0)) {
		char *cad = (char *)malloc(200); strcat(cad,"el identificador: \"");strcat(cad,$1);strcat(cad, "\" no existe");
		yyerror(cad);
		}
		if(!strcmp(getType($1),"int") && !strcmp($3->tipo,"double")){
			char *cad = (char * ) malloc(1000);
			strcat(cad,$1);strcat(cad,"="); strcat(cad,"(int)(");strcat(cad,$3->texto);strcat(cad,")");
			strcat(cad,";");
			$$ = cad;	
		}else if(!strcmp(getType($1),"int") && !strcmp($3->tipo,"int")){
			char *cad = (char * ) malloc(1000);
			strcat(cad,$1);strcat(cad,"="); strcat(cad,$3->texto);
			strcat(cad,";");
			$$ = cad;	
		}else if(!strcmp(getType($1),"double") && !strcmp($3->tipo,"double")){
			char *cad = (char * ) malloc(1000);
			strcat(cad,$1);strcat(cad,"="); strcat(cad,$3->texto);
			strcat(cad,";");
			$$ = cad;	
		}else if(!strcmp(getType($1),"double") && !strcmp($3->tipo,"int")){
			char *cad = (char * ) malloc(1000);
			strcat(cad,$1);strcat(cad,"="); strcat(cad,$3->texto);
			strcat(cad,";");
			$$ = cad;	
		}else
			yyerror("tipos incorrectos");
		
	
	}
	;
operacion
	: IDENTIFIER {
	if(!existe($1,0)) {
		char *cad = (char *)malloc(200); strcat(cad,"el identificador: \"");strcat(cad,$1);strcat(cad, "\" no existe");
		yyerror(cad);
		}
	
	$$ = malloc(sizeof(struct _info)); strcpy($$->tipo,getType($1)); strcpy($$->texto,$1);}
	| IDENTIFIER '+' operacion {
		if((!strcmp(getType($1),"int") || !strcmp(getType($1),"double"))){
				if((!strcmp($3->tipo,"int") || !strcmp($3->tipo,"double"))){
					$$ = malloc(sizeof(struct _info));
					strcat($$->texto,$1);
					strcat($$->texto,"+");
					strcat($$->texto,$3->texto);
					if(!strcmp($3->tipo,"double") || !strcmp(getType($1),"double")){
						strcat($$->tipo,"double");
					}else
						strcat($$->tipo,"int");
					}else
						yyerror("tipos incompatibles");
				}else
					yyerror("tipos incompatibles");
	
	}
	| IDENTIFIER '*' operacion {
		if((!strcmp(getType($1),"int") || !strcmp(getType($1),"double"))){
				if((!strcmp($3->tipo,"int") || !strcmp($3->tipo,"double"))){
					$$ = malloc(sizeof(struct _info));
					strcat($$->texto,$1);
					strcat($$->texto,"*");
					strcat($$->texto,$3->texto);
					if(!strcmp($3->tipo,"double") || !strcmp(getType($1),"double")){
						strcat($$->tipo,"double");
					}else
						strcat($$->tipo,"int");
					}else
						yyerror("tipos incompatibles");
				}else
					yyerror("tipos incompatibles");
	
	}
;
iteration_statement
	: WHILE '(' expression ')' statement { $$ = strcat(strcat(strcat(strcat($1,"("),$3),")"),$5); }
	| DO statement WHILE '(' expression ')' ';' { $$ = strcat(strcat(strcat(strcat(strcat(strcat(strcat($1," "),$2),$3),"("),$5),")"),";"); }
	| FOR '(' expression_statement expression_statement ')' statement 
		{$$ = strcat(strcat(strcat(strcat(strcat($1,"("),$3),$4),")"),$6);  }
	| FOR '(' expression_statement expression_statement expression ')' statement
		{ $$ = strcat(strcat(strcat(strcat(strcat(strcat($1,"("),$3),$4),$5),")"),$7); }
	| FOR '(' declaration expression_statement ')' statement
	| FOR '(' declaration expression_statement expression ')' statement
	;
expression_statement
	: ';' {char *end = (char *)malloc(100); strcat(end,";");$$ = end;}
	| expression ';' { $$ = strcat($1,";");}
	;


%%
void yyerror(char *s)
{
	extern int yylineno;	// predefinida en lex.c
  	extern char *yytext;	// predefinida en lex.c
 	printf("%s  linea %d \n",s,yylineno); 
  	exit(1);
}