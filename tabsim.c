#include "tabsim.h"
#include "stdio.h"
#include "string.h"
int pos = 1;
int iniciado = 0;
stack *s[100];
char *sim[100];

void iniciar() {
	if (!iniciado) {
		s[0] = malloc(sizeof(stack));
		iniciado = 1;
	}
}
void push() {
	iniciar();
	s[pos] = malloc(sizeof(stack));
	pos++;
	//printf("ambito creado\n");
}
void pop() {
	free(s[pos - 1]);
	pos--;
	//printf("ambito cerrado\n");
}

int addSymbol(char *id) {
	iniciar();

	stack *ss = s[pos - 1];
	simbolo **tab = ss->tab;

	while (*tab != NULL) {
		*tab++;
	}
	*tab = malloc(sizeof(simbolo));
	strcpy((*tab)->nombre, id);
	//printf("simbolo insertado: %s\n",(*tab)->nombre);

	return 0;
}

int existe(char *id, int scope) {
	iniciar();
//	printf("recibido: %s\n",id);
	int temp = pos - 1;

	stack *ss = s[temp];
	simbolo **tab = ss->tab;
	while (*tab != NULL) {

		if (!strcmp((*tab)->nombre, id)) {
			return 1;
		}
		*tab++;
	}
	if (!scope) {
		while (temp >= 0) {
			stack *ss = s[temp];
			simbolo **tab = ss->tab;
			while (*tab != NULL) {

				if (!strcmp((*tab)->nombre, id)) {
					return 1;
				}
				*tab++;
			}
			temp--;
		}
	}

	return 0;
}
void setType(char *tipo) {
	iniciar();
	stack *ss = s[pos - 1];
	simbolo **tab = ss->tab;

	while (*tab != NULL) {
		if ((*tab)->tipo[0] == '\0') {
			strcpy((*tab)->tipo, tipo);
			//printf("simbolo: %s\ttipo: %s\n", (*tab)->nombre, (*tab)->tipo);
		} else if ((*tab)->tipo[0] == '*') {
			strcpy((*tab)->tipo, strcat(tipo, (*tab)->tipo));

			//printf("simbolo: %s\ttipo: %s\n", (*tab)->nombre, (*tab)->tipo);
		}
		*tab++;
	}
}

char *getType(char *id) {
	int temp = pos - 1;

		stack *ss = s[temp];
		simbolo **tab = ss->tab;
		while (*tab != NULL) {
			if (!strcmp((*tab)->nombre, id)) {
				return (*tab)->tipo;
			}
			*tab++;
		}
		if (1) {
			while (temp >= 0) {
				stack *ss = s[temp];
				simbolo **tab = ss->tab;
				while (*tab != NULL) {
					if (!strcmp((*tab)->nombre, id)) {
						return (*tab)->tipo;
					}
					*tab++;
				}
				temp--;
			}
		}

		return 0;

}

