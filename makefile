compiler: y.tab.c lex.yy.c main.c
	gcc y.tab.c lex.yy.c main.c -o compiler
y.tab.c: compiler.y
	yacc -d compiler.y -o y.tab.c
	
lex.yy.c: compiler.l
	lex compiler.l
	
clean:
	rm lex.yy.c
	rm y.tab.c
	rm y.tab.h