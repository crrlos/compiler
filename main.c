#include <stdio.h>
#include <stdlib.h>
// prototipo de la funcion parser generada por yacc/bison
int yyparse();

int main(int argc, char **argv)
{
  if ((freopen("/home/carlos/compiler/in.in", "r", stdin) == NULL))
  {
    printf("%s File: %s no puede ser abierto. \n  ",argv[0],argv[1]);
    exit(1);
  }
  yyparse();

  return 0;
}
