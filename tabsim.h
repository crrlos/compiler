#include <stdlib.h>
#include <stdio.h>
#include <string.h>



typedef struct _simbolo {
	char nombre[30];
	char  tipo[30];

} simbolo;

typedef struct _info{
	char texto[1000];
	char tipo[30];
}info;

typedef struct _stack {
	simbolo *tab[100];
} stack;


 simbolo * crear();
 void insertar(simbolo **, simbolo *);
 simbolo *buscar(simbolo *, char[]);
 void imprimir(simbolo * );
 void push();
 void pop();
 int existe(char *,int);
 void setType(char *);
 void iniciar();


